use djangodb;
update django_content_type set app_label = "mywebsite_antispam" where app_label = "antispam";
update django_content_type set app_label = "mywebsite_blog" where app_label = "blog";
update django_content_type set app_label = "mywebsite_home" where app_label = "home";
update django_content_type set app_label = "mywebsite_members" where app_label = "members";
update django_content_type set app_label = "mywebsite_project" where app_label = "project";
