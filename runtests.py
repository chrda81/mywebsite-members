#!/usr/bin/env python
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import os
import sys

if __name__ == "__main__":
    os.environ['DJANGO_SETTINGS_MODULE'] = "tests.settings"

    try:
        import django
        if getattr(django, 'setup', False):
            django.setup()

        from django.conf import settings
        from django.test.utils import get_runner

        TestRunner = get_runner(settings)
        test_runner = TestRunner(verbosity=2, interaction=False, keepdb=False)
        failures = test_runner.run_tests(["tests"])
        sys.exit(bool(failures))

    except ImportError:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        )
