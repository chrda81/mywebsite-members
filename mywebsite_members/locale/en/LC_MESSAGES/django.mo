��    8      �  O   �      �     �     �     �               (     ;     M     a     v     �  
   �  	   �     �     �     �     �     �  	   �       
             $     4     A     Q     b     r     �     �     �     �     �          (     ;     M     [     n     |     �  	   �     �     �     �     �     �     �     �     �  	   
  
     
        *     8  B  F     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  )   �	     
     
  
   $
     /
     7
     D
     Q
     ]
     b
     o
     �
     �
     �
     �
     �
     �
     �
     �
  t   �
     c     h  �   |  {        �     �     �     �     �     �     �     �     �     �     �     �     �     �                         "     '     /             "   #               5           4      -                   ,                     
   	   3                                         !   8   6      +   $   *          7          %      &   0      .   2       1          '   /              (                     )                  CHG_OTP_CANCEL CHG_OTP_DELETE CHG_OTP_HEADING CHG_OTP_SUBMIT CHG_PASSWD_CANCEL CHG_PASSWD_HEADING CHG_PASSWD_SUBMIT EDIT_PROFILE_CANCEL EDIT_PROFILE_HEADING EDIT_PROFILE_SUBMIT LOGOUT_TEXT MENU_ABOUT MENU_BLOG MENU_CATEGORIES MENU_CONTACT MENU_CROWDFUNDING MENU_DATA_PRIVACY MENU_DISCLOSURES MENU_HOME MENU_MEMBERS MENU_TERMS None PROFILE_CHG_OTP PROFILE_EDIT PROFILE_HEADING REGISTER_HEADING REGISTER_SUBMIT RST_COMPLETE_PASSWD_HEADING RST_CONFIRM_PASSWD_HEADING RST_CONFIRM_PASSWD_MESSAGE RST_CONFIRM_PASSWD_SUBMIT RST_PASSWD_DONE_HEADING RST_PASSWD_DONE_MESSAGE1 RST_PASSWD_DONE_MESSAGE2 RST_PASSWD_HEADING RST_PASSWD_SUBMIT address_field company_name_field country_field email email_field fax_field location_field mobile mobile_field phone phone_field preffered_contact_field profile_nr_field profile_title_field url_field user_field uuid_field website_field zipcode_field Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Cancel Delete Change One-Time-Password Save Cancel Change password Save Cancel Edit user profile Save You have successfully logged out. Back to About Blog Categories Contact Crowdfunding Data privacy Disclosures Home Members Area Terms and conditions - Change setting Edit User profile User registration Register Changing password completed Change password The password reset link was invalid, possibly because it has already been used. Please request a new password reset. Save Password reset done I've emailed you instructions for setting your password, if an account exists with the email you entered. You should receive them shortly. If you don't receive an email, please make sure you've entered the address you registered with, and check your spam folder. Forgot password Reset Address Company Name Country E-Mail-Address E-Mail-Address Fax Location Mobil Mobile Phone Phone Preffered Contacting No. Title URL User UUID Website Zipcode 