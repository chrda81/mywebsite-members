��    �      �  �   �      �  �   �  
  �  �   �  W   F  �   �  z   :  1   �  ^   �  q   F  !   �  �   �  /   �  %   �  R   �  e   P  3   �  f   �  �   Q  �     G   �     (     0     B     \     u     �     �     �     �     �     �                    *     9     I     X     j     }     �     �     �     �     �  5   �  
   )     4  	   J     T     `  	   l     v     }  
   �  	   �     �     �     �     �     �  	   �     �  
     
               ?   ,     l     |     �     �     �     �     �     �  =   �     7      W  s   x     �  %        .     6     ?     P     `     |     �     �     �     �     �          )     ;     D     \     s     �     �     �     �     �  B   �  x     k   |  _   �     H  #   O     s     y     �  ~   �  .   	     8     W  (   u     �     �  
   �     �     �     �     �  	   �                    !     .     4     @     X     i     }  	   �  
   �  
   �     �     �  B  �  y   	!  �   �!  �   K"  ]   �"  �   I#  v   $  B   {$  m   �$  �   ,%  %   �%  �   �%  :   �&  6   '  a   ?'  x   �'  9   (  t   T(  �   �(  �   �)  S   �*     �*     �*  #   �*  #   +     :+  #   L+     p+     �+     �+  %   �+     �+     �+     �+  	   ,     ,     ,  	   1,  	   ;,     E,  	   V,     `,  "   q,  	   �,     �,  	   �,  9   �,     �,     -     -  
   -  /   )-     Y-     b-     k-  
   t-     -  
   �-     �-     �-     �-  	   �-     �-     �-     �-     �-     �-     �-  Q   �-     Q.  
   e.     p.     .     �.     �.  $   �.     �.  G   �.  +   ?/  +   k/  z   �/     0  &   10     X0     _0     g0     }0     �0     �0  �   �0  	   P1     Z1  �   r1  �   )2     �2     �2     �2     3  %   3  ,   B3     o3     }3     �3  
   �3     �3  q   �3  �   54  �   �4  �   B5     �5  (   �5     �5     �5     6  x   6  8   �6     �6  #   �6  /   7     77     ?7     Z7  
   g7     r7     w7     �7     �7     �7  
   �7     �7     �7     �7     �7     �7     �7     �7     �7     �7     �7     8     	8     8     =   �       �   �       D   }   H   L       �           �   G   ^       ;       5   b   \   R   7       K       ~       J       X           l                    V   �   C   *             >      �   E   y       9   o          (           i           s   &   4   �   p   d       ,      +   _   t   N   '   c   	   z       e   �   k         u                 �   {      "       Q   `   Y       I          B      j   -       �               @       a                  %   .   [   m               !          2   W      ?         <       
   h   g       �   �       r   n   w   M   0   A      T   3       |          f   $   1   q       x       v      S       #            :   �      F   6   �      /       P   U   )   O   8   ]      Z    
                                Settings for One-Time-Password can be changed using
                                <a href="../otp/">this form</a>.
                                 
                            Raw passwords are not stored, so there is no way to see this
                            user`s password, but you can change the password using
                            <a href="%(the_url)s">this form</a>.
                             
            Forgot your password? Enter your email in the form below and we'll send you instructions for creating a new one.
             
            We have sent an email to %(email)s with further instructions.
             
            We have sent you an email with a link to reset your password. Please check
            your email and click the link to continue.
             
            Your password has been set. You may go ahead and
            <a href="%(the_url)s">login</a>now.
             
    Sincerely,
    %(site_name)s Management
     
    The following user (%(user)s) has asked to register an account at
    %(site_name)s.
     
    To activate this account, please click the following link within the next
    %(expiration_days)s days:
     
    To approve this, please
     
    You (or someone pretending to be you) have asked to register an account at
    %(site_name)s.  If this wasn't you, please ignore this email
    and your address will be removed from our records.
     
    Your account is now approved. You can
     
Sincerely,
%(site_name)s Management
 
The following user (%(user)s) has asked to register an account at
%(site_name)s.
 
To activate this account, please click the following link within the next
%(expiration_days)s days:
 
To approve this, please click the following link.
 
To reset your password, please click the following link, or copy and paste it
into your web browser:
 
You (or someone pretending to be you) have asked to register an account at
%(site_name)s.  If this wasn't you, please ignore this email
and your address will be removed from our records.
 
You are receiving this email because you (or someone pretending to be you)
requested that your password be reset on the %(domain)s site. If you do not
wish to reset your password, please ignore this message.
 
Your account is now approved. You can log in using the following link
 Account Account Activated Account Activation Resent Account Approval failed. Account Approved Account activation failed. Account activation on Account approval on Activation Failure Activation email sent Approval Failure Back to  Best regards CHG_OTP_CANCEL CHG_OTP_DELETE CHG_OTP_HEADING CHG_OTP_SUBMIT CHG_PASSWD_CANCEL CHG_PASSWD_HEADING CHG_PASSWD_SUBMIT Change password Confirm password reset EDIT_PROFILE_CANCEL EDIT_PROFILE_HEADING EDIT_PROFILE_SUBMIT Enter your new password below to reset your password: First name Forgot your password? Greetings Issuer name LOGOUT_TEXT Last name Log in Log out MENU_ABOUT MENU_BLOG MENU_CATEGORIES MENU_CONTACT MENU_CROWDFUNDING MENU_DATA_PRIVACY MENU_DISCLOSURES MENU_HOME MENU_MEMBERS MENU_TERMS Management None Not a member? Once a site administrator activates your account you can login. PROFILE_CHG_OTP PROFILE_EDIT PROFILE_HEADING Password Password changed Password reset Password reset complete Password successfully changed! Please check your email to complete the registration process. Please correct the error below. Please correct the errors below. Please enter a correct %(username)s, password, and 2FA code. Note that either or both fields may be case-sensitive. Please provide a 2 FA Code. Please provide the correct 2 FA Code. Profile Profiles REGISTER_HEADING REGISTER_SUBMIT RST_COMPLETE_PASSWD_HEADING RST_CONFIRM_PASSWD_HEADING RST_CONFIRM_PASSWD_MESSAGE RST_CONFIRM_PASSWD_SUBMIT RST_PASSWD_DONE_HEADING RST_PASSWD_DONE_MESSAGE1 RST_PASSWD_DONE_MESSAGE2 RST_PASSWD_HEADING RST_PASSWD_SUBMIT Register Register for an account Registration is closed Resend Activation Email Reset it Reset password Secret Security Set password Sorry, but registration is closed at this moment. Come back later. Step 1: Choose your issuer name, e.g. name of this homepage, organization, etc. Please don't use any special characters! Step 2: Add your secret as TOTP code to your authentication app, e.g. Google Authenticator, 1Password, etc. Step 3: Use the code displayed on your authentication app, to verify your settings when saving. Submit The user's account is now approved. Title Username Website You are authenticated as %(user)s, but are not authorized to access this page. Would you like to login to a different account? You may now <a href="%(login_url)s">log in</a> Your account is now activated. Your password has been reset! Your username, in case you've forgotten: address_field admin approval click here company_name_field country_field email email_field fax_field location_field log in. mobile mobile_field phone phone_field preffered_contact_field profile_nr_field profile_title_field registration url_field user_field uuid_field website_field zipcode_field Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
Einstellungen zum One-Time-Password können mit <a href="../otp/">diesem Formular</a> geändert werden.
                 
Die Passwörter werden nicht im Klartext gespeichert und können daher nicht dargestellt, sondern nur mit <a href="%(the_url)s">diesem Formular</a> geändert werden.
                                 
            Passwort vergessen? Gib deine Email-Adresse in die untere Zeile ein und es werden dir Anweisungen zum Erstellen eines Neuen gesendet.
             
            Es wurde eine Email an %(email)s mit weiteren Anweisungen gesendet.
             
            Dir wurde eine Email mit einem Link geschickt, um dein Password zurückzusetzen. Bitte überprüfe
            deine Emails und klicke den Link um fortzufahren.
             
Dein Passwort wurde festgelegt. Du kannst nun weitermachen und dich
<a href="%(the_url)s">einloggen</a>.
             
    Mit freundlichen Grüßen
    Das Team von %(site_name)s
     
    Der folgende Benutzer (%(user)s) möchte einen Account auf der Seite
    %(site_name) registrieren.
     
    Um diesen Account zu aktivieren, klicke bitte auf den folgenden Link innerhalb der nächsten
    %(expiration_days)s Tage:
     
    Um das zu genehmigen, bitte
     
    Du (oder jemand der vorgibt Du zu sein) hat beantragt einen Account auf %(site_name)s
    zu registrieren. Falls du es nicht gewesen bist, kannst du diese Email ignorieren
    und deine Daten werden bei uns schnellstmöglich gelöscht.
     
    Dein Account wurde nun genehmigt. Du kannst dich
     
Mit freundlichen Grüßen
Das Team von %(site_name)s
 
Der folgende Benutzer (%(user)s) möchte einen Account auf der Seite
%(site_name) registrieren.
 
Um diesen Account zu aktivieren, klicke bitte auf den folgenden Link innerhalb der nächsten
%(expiration_days)s Tage:
 
Um das zu genehmigen, bitte auf folgenden Link klicken.
 
Um dein Passwort zurückzusetzen klicke bitte auf den folgenden Link, oder kopiere diesen in
in deinen Webbrowser:
 
Du (oder jemand der vorgibt Du zu sein) hat beantragt einen Account auf %(site_name)s
zu registrieren. Falls du es nicht gewesen bist, kannst du diese Email ignorieren
und deine Daten werden bei uns schnellstmöglich gelöscht.
 
Du erhältst diese Email, weil Du (oder jemand der vorgibt Du zu sein)
beantragt hat, dein Passwort auf der Seite %(domain)s zurückzusetzen. Falls du es nicht
ändern möchtest, kannst du diese Email ignorieren.
 
Dein Account wurde nun genehmigt. Du kannst dich nun mit folgendem Link einloggen
 Konto Account aktiviert Account-Aktivierung erneut gesendet Account-Genehmigung fehlgeschlagen. Account genehmigt Account-Aktivierung fehlgeschlagen. Account-Aktivierung an Account-Genehmigung an Aktivierungsfehler Email zur Aktivierung wurde versendet Genehmigungsfehler Zurück zu  Viele Grüße Abbrechen Löschen One-Time-Password ändern Speichern Abbrechen Passwort ändern Speichern Passwort ändern Passwort-Zurücksetzen bestätigen Abbrechen Benutzerprofil bearbeiten Speichern Gib dein neues Passwort unten ein, um es zurückzusetzen: Vorname Passwort vergessen? Hallo Aussteller Du hast dich erfolgreich abgemeldet. Zurück zu Nachname Anmelden Abmelden Über Mich Blog Kategorien Kontakt Crowdfunding Datenschutz Impressum Home Mitgliederbereich AGB Das Serviceteam - Kein Account vorhanden? Nachdem ein Administrator deinen Account aktiviert hat, kannst du dich einloggen. Einstellung ändern Bearbeiten Benutzerprofil Passwort Passwort geändert Passwort zurücksetzen Passwort-Zurücksetzen abgeschlossen Passwort erfolgreich geändert! Bitte prüfe deine Emails, um den Registrierungsprozess abzuschließen. Bitte korrigiere den untenstehenden Fehler. Bitte korrigiere den untenstehenden Fehler. Bitte gib einen korrekten %(username)s, Passwort, und 2FA Code ein. Beachte, dass die Eingaben case-sensitiv sein können. Bitte gib einen 2 FA Code ein. Bitte gib den korrekten 2 FA Code ein. Profil Profile Benutzer registrieren Registrieren Passwort erfolgreich geändert Passwort ändern Der Link zum Zurücksetzen des Passworts war ungültig, möglicherweise weil er bereits verwendet wurde. Bitte fordere einen neuen Passwort-Reset an. Speichern Passwort zurückgesetzt Ich habe Dir eine Anleitung zur Einrichtung Deines Passworts per E-Mail geschickt, wenn ein Account mit der von Dir eingegebenen E-Mail existiert. Du solltest sie in Kürze erhalten. Wenn Du keine E-Mail erhältst, vergewissere dich bitte, dass Du die Adresse eingegeben hast, mit der Du dich registriert hast, und überprüfe bitte Deinen Spam-Ordner. Passwort vergessen Zurücksetzen Registrieren Neuen Account registrieren Registrierung zur Zeit nicht möglich Email für Account-Aktivierung erneut senden Zurücksetzen Passwort zurücksetzen Geheimer Schlüssel Sicherheit Passwort setzen Es tut uns leid, aber leider ist die Registrierung aktuell nicht möglich. Bitte versuche es später noch einmal. Schritt 1: Wähle einen Namen für den Aussteller, z.B. den Namen der Homepage, Organisation, o.ä. Bitte benutze keine Sonderzeichen! Schritt 2: Füge deinen geheimen Schlüssel als TOTP Code deiner Authenticator App hinzu, z.B. Google Authenticator, 1Password, o.ä. Schritt 3: Benutze den angezeigten Code auf deiner Authenticator App, um deine Einstellungen während des Speicherns zu verifizieren. Senden Der Benutzeraccount wurde nun genehmigt. Anrede Benutzer Webseite Du bist als %(user)s angemeldet, aber nicht für diese Seite authorisiert. Möchtest Du einen anderen Account versuchen? Du kannst dich nun <a href="%(login_url)s">einloggen</a> Dein Account ist nun aktiviert. Dein Passwort wurde zurückgesetzt! Dein Benutzername, falls du ihn vergessen hast: Adresse Administrative Genehmigung hier klicken Firmenname Land E-Mail-Adresse E-Mail-Adresse Fax Ort einloggen. Handy Handy Telefon Telefon Bevorzugte Kontaktaufnahme Nr Anrede Registrierung URL Benutzer UUID Webseite Postleitzahl 