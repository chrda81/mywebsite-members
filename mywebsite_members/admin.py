# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

from mywebsite.admin import CustomUserAdmin
from phonenumber_field.modelfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberInternationalFallbackWidget, PhoneNumberPrefixWidget
from related_admin import RelatedFieldAdmin

from .models import Profile

# User model alias
User = get_user_model()

# Unregister the provided model admin
admin.site.unregister(User)


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = _('Profile')
    fk_name = 'user'
    formfield_overrides = {
        PhoneNumberField: {'widget': PhoneNumberPrefixWidget},
    }


class UserAdmin(RelatedFieldAdmin, CustomUserAdmin):
    inlines = (ProfileInline, )
    list_display = ('username', 'email', 'first_name', 'last_name', 'profile__nr', 'is_staff', 'is_active')
    search_fields = ('username', 'first_name', 'last_name', 'email', 'profile__nr')
    list_editable = ('is_staff', 'is_active')
    list_select_related = ('profile', )

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(UserAdmin, self).get_inline_instances(request, obj)


admin.site.register(User, UserAdmin)
