# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import json
from datetime import datetime

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth import views as auth_views
from django.contrib.auth.forms import PasswordResetForm
from django.core import mail
from django.test import RequestFactory, TestCase
from django.urls import resolve, reverse

import requests

# User model alias
User = get_user_model()


class SourceCodeAPITests(TestCase):
    def setUp(self):
        super(SourceCodeAPITests, self).setUp()

    def test_api_connection(self):
        if settings.SOURCE_CODE_API_USE:
            api_json = settings.SOURCE_CODE_API_TEMPLATE.substitute(
                dict(
                    id=datetime.now().strftime('%Y%m%d%H%M%S%f'),
                    userid='test_user',
                    active=json.dumps(False)
                ))
            api_json = json.loads(api_json)
            r = requests.get(settings.SOURCE_CODE_API_URL, json=api_json)
            resp = r.json()

            if resp['error'] is None:
                msg = resp['result']['msg']

            if resp['result'] is None:
                msg = resp['error']

            # This test expects using the RhodeCode API. If you use any other source code API, please
            # verify the assertion string
            self.assertIn('user `test_user` does not exist', msg)


class URLTests(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        self.username = 'john'
        self.password = 'glass0nion'
        self.email = 'jlennon@beatles.com'
        self.user = User.objects.create_user(
            username=self.username, email=self.email, password=self.password)
        super(URLTests, self).setUp()

    def test_redirects_to_login_page_on_not_loggedin(self):
        response = self.client.get(settings.LOGIN_REDIRECT_URL, follow=True)
        self.assertRedirects(response, '{}?next={}'.format(settings.LOGIN_URL, settings.LOGIN_REDIRECT_URL))

    def test_redirects_to_test_page_on_loggedin(self):
        self.client.login(username=self.user, password=self.password)
        response = self.client.post(settings.LOGIN_URL, {
            'username': self.user,
            'password': self.password
        })
        self.assertRedirects(response, settings.LOGIN_REDIRECT_URL, fetch_redirect_response=False)


class PasswordResetTests(TestCase):
    def setUp(self):
        url = reverse('members:password_reset')
        self.response = self.client.get(url)
        super(PasswordResetTests, self).setUp()

    def test_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_view_function(self):
        view = resolve('/members/password/reset/')
        self.assertEqual(view.func.view_class, auth_views.PasswordResetView)

    def test_csrf(self):
        self.assertContains(self.response, 'csrfmiddlewaretoken')

    def test_contains_form(self):
        form = self.response.context.get('form')
        self.assertIsInstance(form, PasswordResetForm)

    # def test_form_inputs(self):
    #     '''
    #     The view must contain two inputs: csrf and email
    #     '''
    #     self.assertContains(self.response, '<input', 2)
    #     self.assertContains(self.response, 'type="email"', 1)


class SuccessfulPasswordResetTests(TestCase):
    def setUp(self):
        email = 'john@doe.com'
        User.objects.create_user(username='john', email=email, password='123abcdef')
        url = reverse('members:password_reset')
        self.response = self.client.post(url, {'email': email})
        super(SuccessfulPasswordResetTests, self).setUp()

    def test_redirection(self):
        '''
        A valid form submission should redirect the user to `password_reset_done` view
        '''
        url = reverse('members:password_reset_done')
        self.assertRedirects(self.response, url)

    def test_send_password_reset_email(self):
        self.assertEqual(1, len(mail.outbox))


class InvalidPasswordResetTests(TestCase):
    def setUp(self):
        url = reverse('members:password_reset')
        self.response = self.client.post(url, {'email': 'donotexist@email.com'})
        super(InvalidPasswordResetTests, self).setUp()

    def test_redirection(self):
        '''
        Even invalid emails in the database should
        redirect the user to `password_reset_done` view
        '''
        url = reverse('members:password_reset_done')
        self.assertRedirects(self.response, url)

    def test_no_reset_email_sent(self):
        self.assertEqual(0, len(mail.outbox))
