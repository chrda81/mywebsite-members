# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2021 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

# Patches Django-Phonenumber-Field widget.

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.forms import Select
from django.utils import translation

from phonenumber_field import widgets as phonenumber_field_widgets
from phonenumbers.data import _COUNTRY_CODE_TO_REGION_CODE

try:
    import babel
except ModuleNotFoundError:
    babel = None


class PhonePrefixSelect(Select):
    initial = None

    def __init__(self, initial=None):
        if babel is None:
            raise ImproperlyConfigured(
                "The PhonePrefixSelect widget requires the babel package be installed."
            )

        choices = [("", "---------")]
        language = translation.get_language() or settings.LANGUAGE_CODE
        locale = babel.Locale(translation.to_locale(language))

        # get configured countries from settings
        possible_countries = getattr(settings, "PHONENUMBER_DEFAULT_COUNTRIES", None)

        if not initial:
            initial = getattr(settings, "PHONENUMBER_DEFAULT_REGION", None)
        for prefix, values in _COUNTRY_CODE_TO_REGION_CODE.items():
            prefix = "+%d" % prefix
            if initial and initial in values:
                self.initial = prefix
            for country_code in values:
                country_name = locale.territories.get(country_code)
                if country_name:
                    # compare against configured countries from settings
                    if possible_countries and country_name not in possible_countries:
                        continue
                    choices.append((prefix, "{} {}".format(country_name, prefix)))
        super().__init__(choices=sorted(choices, key=lambda item: item[1]))

    def get_context(self, name, value, attrs):
        return super().get_context(name, value or self.initial, attrs)


# monkey-patch the classes from django-phonenumber-field
phonenumber_field_widgets.PhonePrefixSelect = PhonePrefixSelect
