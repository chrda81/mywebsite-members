# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import uuid

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django_otp.models import OTPSecrets
from phonenumber_field.modelfields import PhoneNumberField

# User model alias
User = get_user_model()


# Define states for PREFERRED_CONTACT_CHOICES
PREFERRED_CONTACT_CHOICES = (
    ('m', _('mobile')),
    ('p', _('phone')),
    ('e', _('email'))
)


class Profile(models.Model):
    nr = models.CharField(
        max_length=25,
        blank=True,
        null=True,
        unique=True,
        verbose_name=_('profile_nr_field')
    )
    user = models.OneToOneField(
        User,
        related_name='profile',
        on_delete=models.CASCADE,
        verbose_name=_('user_field')
    )
    title = models.CharField(
        max_length=25,
        blank=True,
        null=True,
        verbose_name=_('profile_title_field')
    )
    uuid = models.UUIDField(
        default=uuid.uuid4,
        editable=False,
        verbose_name=_('uuid_field')
    )
    company_name = models.CharField(
        max_length=250,
        verbose_name=_('company_name_field'),
        blank=True,
        null=True,
    )
    title = models.CharField(
        max_length=25,
        blank=True,
        null=True,
        verbose_name=_('profile_title_field')
    )
    website = models.CharField(
        max_length=250,
        blank=True,
        null=True,
        verbose_name=_('website_field')
    )
    mobile = PhoneNumberField(
        blank=True,
        null=True,
        verbose_name=_('mobile_field')
    )
    phone = PhoneNumberField(
        blank=True,
        null=True,
        verbose_name=_('phone_field')
    )
    fax = PhoneNumberField(
        blank=True,
        null=True,
        verbose_name=_('fax_field')
    )
    preferred_contact = models.CharField(
        max_length=6,
        choices=PREFERRED_CONTACT_CHOICES,
        blank=True,
        null=True,
        verbose_name=_('preffered_contact_field')
    )
    country = models.CharField(
        max_length=128,
        blank=True,
        null=True,
        verbose_name=_('country_field')
    )
    zipcode = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        verbose_name=_('zipcode_field')
    )
    location = models.CharField(
        max_length=250,
        blank=True,
        null=True,
        verbose_name=_('location_field')
    )
    address = models.CharField(
        max_length=250,
        blank=True,
        null=True,
        verbose_name=_('address_field')
    )

    class Meta:
        db_table = 'members_profile'
        ordering = ['user']
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')

    def __str__(self):  # __unicode__ for Python 2
        return self.user.username
