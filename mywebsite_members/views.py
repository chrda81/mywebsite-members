# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import (authenticate, get_user_model, login,
                                 update_session_auth_hash)
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.db import transaction
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django_otp.models import OTPSecrets
from pyotp import TOTP
from registration.backends.admin_approval import views as approval_views

from .forms import (AuthenticationForm, OTPGenerationForm, ProfileForm,
                    UserForm, UserRegistrationForm)

if settings.REGISTRATION_USE_ADMIN_APPROVAL:
    from registration.backends.admin_approval import views as registration_views
else:
    from registration.backends.default import views as registration_views


# User model alias
User = get_user_model()


# def register(request):
#     if request.method == 'POST':
#         form = UserRegistrationForm(request.POST)

#         if form.is_valid():
#             userObj = form.cleaned_data
#             username = userObj['username']
#             password = userObj['password1']
#             form.save()
#             # login user
#             user = authenticate(username=username, password=password)
#             login(request, user)
#             return redirect('members:profile')
#         elif form.has_error('url', code='spam-protection') or form.has_error('message', code='spam-protection'):
#             # Fake successful creation
#             return redirect('members:profile')
#         else:
#             messages.error(request, _('Please correct the error below.'))
#     else:
#         form = UserRegistrationForm()

#     return render(request, 'members/register.html', {'form': form})


@login_required(login_url='/members/login/')
def profile(request):
    user = request.user
    profile = request.user.profile
    args = {
        'user': user,
        'profile': profile,
    }
    if hasattr(request.user, 'otp_secret'):
        args.update({'otp_enabled': 'otp_is_enabled'})
    else:
        args.update({'otp_enabled': 'otp_is_disabled'})

    return render(request, 'members/profile.html', args)


@login_required(login_url='/members/login/')
@transaction.atomic
def edit_profile(request):
    if request.method == 'POST':
        form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, instance=request.user.profile)

        if form.is_valid() and profile_form.is_valid():
            form.save()
            profile_form.save()
            return redirect('members:profile')
        else:
            messages.error(request, _('Please correct the error below.'))

    else:
        form = UserForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)

    args = {
        'form': form,
        'profile_form': profile_form
    }
    return render(request, 'members/edit_profile.html', args)


@login_required(login_url='/members/login/')
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)

        if form.is_valid():
            form.save()
            # leave user logged in, after changing password
            update_session_auth_hash(request, form.user)
            return redirect('members:profile')
        else:
            messages.error(request, _('Please correct the error below.'))

    else:
        form = PasswordChangeForm(user=request.user)

    args = {'form': form}
    return render(request, 'members/change_password.html', args)


@login_required(login_url='/members/login/')
def change_otp(request):
    if request.method == 'POST':
        form = OTPGenerationForm(data=request.POST, instance=request.user.otp_secret)

        if "cancel" in request.POST:
            otp_secret = OTPSecrets.objects.get(user_id=request.user)
            # delete unsaved otp_secret
            if otp_secret.secret == '':
                OTPSecrets.objects.get(user_id=request.user).delete()
            return redirect('members:edit_profile')

        if "delete" in request.POST:
            OTPSecrets.objects.get(user_id=request.user).delete()
            return redirect('members:edit_profile')

        if form.is_valid():
            otp_secret = form.cleaned_data['secret']
            otp_auth = form.cleaned_data['otp_auth']
            if otp_auth != '':
                totp = TOTP(otp_secret)

                if totp.verify(otp_auth):
                    form.save()
                    return redirect('members:edit_profile')
                else:
                    messages.error(request, _('Please provide the correct 2 FA Code.'))
            else:
                messages.error(request, _('Please provide a 2 FA Code.'))

        else:
            messages.error(request, _('Please correct the error below.'))

    else:
        # create new otp, if it doesn't exist
        if not hasattr(request.user, 'otp_secret'):
            OTPSecrets.objects.create(user=request.user)

        form = OTPGenerationForm(instance=request.user.otp_secret)

    args = {'form': form}
    return render(request, 'members/change_otp.html', args)


class LoginView(auth_views.LoginView):
    form_class = AuthenticationForm
    template_name = 'django_otp/member_login.html'


class ActivationView(registration_views.ActivationView):
    """Override success_url from ActivationView of registration.backends views"""

    def get_success_url(self, user):
        return ('members:registration_activation_complete', (), {})


class ApprovalView(approval_views.ApprovalView):
    """Override success_url from ApprovalView of registration.backends.admin_approval.views"""

    def get_success_url(self, user):
        return ('members:registration_activation_complete', (), {})
