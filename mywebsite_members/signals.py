# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import json
import sys
from datetime import datetime

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import Signal, receiver

import requests

from .models import Profile

# User model alias
User = get_user_model()

# define custom signals
user_creation_done = Signal(providing_args=["instance"])


# subscribe to event post_save of table 'user'
@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance=None, created=False, **kwargs):
    if created or not hasattr(instance, 'profile'):
        Profile.objects.create(user=instance)
    instance.profile.save()
    # send the signal
    user_creation_done.send(sender=sender, instance=instance)
