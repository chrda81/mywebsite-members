# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class MembersConfig(AppConfig):
    """The default AppConfig for the website module ``mywebsite_members``."""
    name = 'mywebsite_members'
    verbose_name = _("Website Members")

    def ready(self):
        """ Apply monkey patches """
        import mywebsite_members.monkey_patches.django_phonenumber_field  # monkey-patch the classes from django-phonenumber-field
        """Wire up the signals """
        import mywebsite_members.signals
