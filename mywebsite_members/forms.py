# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import re

from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django_otp.forms import AuthenticationForm as DjangoAuthForm
from django_otp.models import OTPSecrets
from django_otp.widgets import OTPGenWidget
from mywebsite_antispam.honeypot.forms import HoneypotField
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import (PhoneNumberInternationalFallbackWidget,
                                       PhoneNumberPrefixWidget)

from .models import Profile

try:
    from urllib.parse import urlencode
except ImportError:
    from urllib import urlencode  # noqa

# User model alias
User = get_user_model()


class AuthenticationForm(DjangoAuthForm):
    def __new__(cls, *args, **kwargs):
        """Class creation control."""
        instance = super(AuthenticationForm, cls).__new__(cls)
        instance.error_messages["invalid_login"] = _(
            "Please enter a correct %(username)s, password, and 2FA code. "
            "Note that either or both fields may be case-sensitive."
        )
        return instance


class UserRegistrationForm(UserCreationForm):
    title = forms.CharField(required=False, max_length=25)
    email = forms.EmailField(required=True)
    url = HoneypotField()

    class Meta:
        model = User
        fields = (
            'title',
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'
        )
        labels = {
            'title': _('profile_title_field'),
            'email': _('email_field'),
            'url': _('url_field')
        }

    def save(self, commit=True):
        user = super(UserRegistrationForm, self).save(commit=False)
        user.username = self.cleaned_data['username']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']

        if commit:
            user.save()
            user_profile = Profile.objects.get(user=user)
            # user_profile.preferred_contact = None
            user_profile.save()

        return user, user_profile


class UserForm(UserChangeForm):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'username',
            'email',
            'password',
        )

    def __init__(self, *args, **kwargs):
        """Init the instance."""
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True


class ProfileForm(forms.ModelForm):
    mobile = PhoneNumberField(widget=PhoneNumberPrefixWidget(), required=False)
    phone = PhoneNumberField(widget=PhoneNumberPrefixWidget(), required=False)
    fax = PhoneNumberField(widget=PhoneNumberPrefixWidget(), required=False)

    class Meta:
        model = Profile
        fields = (
            'title',
            'company_name',
            'address',
            'zipcode',
            'location',
            'country',
            'website',
            'mobile',
            'phone',
            'fax',
            'preferred_contact',
        )


class OTPGenerationForm(forms.ModelForm):
    """2 FA code generation form."""
    otp_auth = forms.CharField(label="2 FA Code", max_length=6, required=False,
                               help_text=_("Step 3: Use the code displayed on your authentication app, to verify your "
                                           "settings when saving."))

    class Meta:
        model = OTPSecrets
        fields = (
            'issuer_name',
            'secret',
            'otp_auth',
        )
        labels = {
            'issuer_name': _("Issuer name"),
            'secret': _("Secret"),
        }
        widgets = {
            "secret": OTPGenWidget(embed_script=True, attrs={'class': 'secret'}),
        }
        help_texts = {
            "issuer_name": _("Step 1: Choose your issuer name, e.g. name of this homepage, organization, etc. "
                             "Please don't use any special characters!"),
            "secret": _("Step 2: Add your secret as TOTP code to your authentication app, e.g. Google Authenticator, "
                        "1Password, etc."),
        }

    def __init__(self, *args, **kwargs):
        """Init the instance."""
        super(OTPGenerationForm, self).__init__(*args, **kwargs)
        self.fields['otp_auth'].widget.attrs.update({
            'autocomplete': 'off'
        })

        def fire_gen(gen_new=True):
            return (
                "assignOTPSecret("
                "'{0}',"
                "document.querySelector('#{1}_container .secret'),"
                "document.querySelector('#{1}_container .qrcode'),"
                "'{2}', document.querySelector('#{3}').value,{4}"
                ");"
            ).format(
                self.instance.user.username,
                self["secret"].id_for_label,
                re.sub(r"/A{16}$", "", reverse(
                    "django_otp:qrcode",
                    kwargs={"secret": "AAAAAAAAAAAAAAAA"}
                )),
                self["issuer_name"].id_for_label,
                str(gen_new).lower()
            )
        self.fields["issuer_name"].widget.attrs["onblur"] = fire_gen(False)
        self.fields["secret"].widget.btn_attrs["onclick"] = fire_gen()
        self.fields["secret"].widget.img_attrs.setdefault("class", "")
        self.fields["secret"].widget.img_attrs["class"] += " qrcode"
        if self.instance.secret:
            self.fields["secret"].widget.img_attrs["src"] = reverse(
                "django_otp:qrcode", kwargs={"secret": self.instance.secret}
            ) + ("?{}").format(urlencode({
                key: value
                for (key, value) in {
                    "name": self.instance.user.username,
                    "issuer_name": self.instance.issuer_name
                }.items() if value
            }))
