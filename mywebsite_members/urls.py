# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.conf import settings
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import permission_required
from django.urls import include, path, re_path, reverse_lazy
from django.views.generic.base import TemplateView

from . import views

if settings.REGISTRATION_USE_ADMIN_APPROVAL:
    from registration.backends.admin_approval import views as registration_views
else:
    from registration.backends.default import views as registration_views


app_name = 'members'

urlpatterns = [
    # customized urls from registration.backends.default.urls
    path('activate/resend/', registration_views.ResendActivationView.as_view(), name='registration_resend_activation'),
    path('activate/complete/', TemplateView.as_view(template_name='registration/activation_complete.html',),
         name='registration_activation_complete'),
    # Activation keys get matched by \w+ instead of the more specific
    # [a-fA-F0-9]{40} because a bad activation key should still get to the view;
    # that way it can return a sensible "invalid key" message instead of a
    # confusing 404.
    re_path(r'^activate/(?P<activation_key>\w+)/$', views.ActivationView.as_view(), name='registration_activate'),
    path('approve/complete/', TemplateView.as_view(template_name='registration/admin_approve_complete.html'),
         name='registration_approve_complete'),
    re_path(r'^approve/(?P<profile_id>[0-9]+)/$', permission_required('is_superuser')(views.ApprovalView.as_view()),
        name='registration_admin_approve'),
    path('register/complete/', TemplateView.as_view(template_name='registration/registration_complete.html'),
         name='registration_complete'),
    path('register/closed/', TemplateView.as_view(template_name='registration/registration_closed.html'),
         name='registration_disallowed'),
    path('register/', registration_views.RegistrationView.as_view(
        disallowed_url=reverse_lazy('members:registration_disallowed'),
        success_url=reverse_lazy('members:registration_complete')),
        name='registration_register'),
    # path('register/', views.register, name='register'),
    path('profile/', views.profile, name='profile'),
    path('profile/edit/', views.edit_profile, name='edit_profile'),
    path('profile/password/', views.change_password, name='change_password'),
    path('profile/otp/', views.change_otp, name='change_otp'),
    # customized urls from registration.auth_urls
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='registration/logout.html'), name='logout'),
    path('password/change/', auth_views.PasswordChangeView.as_view(
        success_url=reverse_lazy('members:password_change_done')),
        name='password_change'),
    path('password/change/done/', auth_views.PasswordChangeDoneView.as_view(),
         name='password_change_done'),
    path('password/reset/', auth_views.PasswordResetView.as_view(
        success_url=reverse_lazy('members:password_reset_done')),
        name='password_reset'),
    path('password/reset/complete/', auth_views.PasswordResetCompleteView.as_view(),
         name='password_reset_complete'),
    path('password/reset/done/', auth_views.PasswordResetDoneView.as_view(),
         name='password_reset_done'),
    re_path(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
            auth_views.PasswordResetConfirmView.as_view(
                success_url=reverse_lazy('members:password_reset_complete')),
            name='password_reset_confirm'),
]
