"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import os

from django.conf import settings
from mywebsite.settings.settings import *
from test_website.settings.base import *
from split_settings.tools import include, optional

include(
    optional('../test_website/settings/settings_secure.py'),
)

# set BASE_DIR again
settings.BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = True

# override settings for DATABASE
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(settings.BASE_DIR, 'testdb.sqlite3'),
    }
}

TEST_RUNNER = 'tests.test_runners.DefaultDbTestRunner'

FIXTURE_DIRS = (
    os.path.join(settings.BASE_DIR, 'fixtures/'),
)

# SQLite backend does not support timezone-aware datetimes when USE_TZ is False.
USE_TZ = True

# DBBackup Settings
# https://github.com/django-dbbackup/django-dbbackup
# DBBACKUP_STORAGE = 'django.core.files.storage.FileSystemStorage'
# DBBACKUP_STORAGE_OPTIONS = {'location': '/var/backups'}
